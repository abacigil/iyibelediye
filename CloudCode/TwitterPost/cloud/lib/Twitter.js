module.exports = {
    RestClient: require('cloud/lib/RestClient'),
    SearchClient: require('cloud/lib/SearchClient'),
    StreamClient: require('cloud/lib/StreamClient')
};
