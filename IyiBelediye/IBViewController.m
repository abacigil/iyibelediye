//
//  IBViewController.m
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "IBViewController.h"
#import "ExampleViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+Extensions.h"
#import "IBBelediyeTableViewController.h"
@interface IBViewController ()

@end

@implementation IBViewController
-(UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad
{

    self.title = @"Rapor Tipi";
    self.reports = [NSMutableArray array];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *query = [PFQuery queryWithClassName:@"Report"];
    [query orderByAscending:@"Order"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            
            [self.reports addObjectsFromArray:objects];
           
            [self.tableView reloadData];
            
        
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
        } else {
            NSString *errorStr = [NSString stringWithFormat:@"Error: %@ %@", error, [error userInfo]];
            [[FlightRecorder sharedInstance] logAPIRequestWithName:@"Load Reports" url:@"Report" httpMethod:@"GET" requestBody:nil requestHeaders:nil responseStatusCode:500 responseString:errorStr responseHeaders:nil];
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.reports.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 142;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    PFObject *object = [self.reports objectAtIndex:indexPath.row];
    
    UITableViewCell *cell;
    
    
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"reportCell"];
    
    
    UILabel *btn = (UILabel *)[cell viewWithTag:100];
    btn.text = [object objectForKey:@"ReportType"];
    [cell setBackgroundColor:[UIColor colorWithHEX:[object objectForKey:@"Color"]]];
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     PFObject *object = [self.reports objectAtIndex:indexPath.row];
    
        IBBelediyeTableViewController *belediyeView = [self.storyboard instantiateViewControllerWithIdentifier:@"IBBelediyeTableViewController"];
    belediyeView.selectedReport =object;
        [self.navigationController pushViewController:belediyeView animated:YES];
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"UI Actions" action:@"TableView Row" label:@"Report Selected" value:[object objectForKey:@"ReportType"]];
    
}



-(void)viewDidAppear:(BOOL)animated
{
    // Check if user is logged in
    if (![PFUser currentUser]) {
        // If not logged in, we will show a PFLogInViewController
        PFLogInViewController *logInViewController = [[PFLogInViewController alloc] init];
        
        
        // Customize the Log In View Controller
        logInViewController.delegate = self;
        logInViewController.facebookPermissions = @[@"email"];
        logInViewController.fields =  PFLogInFieldsFacebook | PFLogInFieldsTwitter;
        // Show Twitter login, Facebook login, and a Dismiss button.
        
        logInViewController.logInView.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iyibelediye120"]];
        
//        logInViewController.signUpController.signUpView.emailField.placeholder = @"E-posta adresin";
//        logInViewController.signUpController.signUpView.passwordField.placeholder = @"Şifren";
//        logInViewController.signUpController.signUpView.usernameField.placeholder = @"Kullanıcı adın";
//        logInViewController.signUpController.signUpView.logo = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"iyibelediye120"]];
//        [logInViewController.signUpController.signUpView.signUpButton setTitle:@"Kayıt Ol" forState:UIControlStateNormal];
//        
//        
//        [logInViewController.logInView.signUpButton setTitle:@"Kayıt Ol" forState:UIControlStateNormal];
//        logInViewController.logInView.passwordField.placeholder = @"Şifren";
//        logInViewController.logInView.usernameField.placeholder = @"Kullanıcı adın";
//        [logInViewController.logInView.passwordForgottenButton setTitle:@"Unuttum" forState:UIControlStateNormal];
//        logInViewController.logInView.externalLogInLabel.text = @"Facebook veya Twitter ile giriş yap";
//        logInViewController.logInView.signUpLabel.text = @"Kayıt olmak mı istiyorsun?";
        
        // Present Log In View Controller
        [self presentViewController:logInViewController animated:YES completion:^{
            
            ExampleViewController *vv = [[ExampleViewController alloc] init];
            
            [logInViewController presentViewController:vv animated:YES completion:nil];
            
        }];
        
        
        [[FlightRecorder sharedInstance] trackPageView:@"Login View"];
        
        
        
    }
    else
    {
        [[FlightRecorder sharedInstance] trackPageView:@"Report View"];
    }
    
    [super viewDidAppear:animated];
}


#pragma mark - PFLogInViewControllerDelegate

// Sent to the delegate when a PFUser is logged in.
- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user {
    
    if ([PFUser currentUser]) {
        
        if([PFUser currentUser].username != nil)
        {
            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].username];
            [[FlightRecorder sharedInstance] logWithName:@"Login Success" value:[PFUser currentUser].username];
        }
        else
        {
            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].email];
            [[FlightRecorder sharedInstance] logWithName:@"Login Success" value:[PFUser currentUser].email];
        }
        
    }
    
    
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
    }];
}

// Sent to the delegate when the log in attempt fails.
- (void)logInViewController:(PFLogInViewController *)logInController didFailToLogInWithError:(NSError *)error {
    
    if(error != nil)
    {
        [[FlightRecorder sharedInstance] warningWithName:@"Login Fail" value:error.description];
        
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Error" label:@"System Error" value:error.description];
    }
    else
    {
        [[FlightRecorder sharedInstance] warningWithName:@"Login Fail" value:@"No description"];
        
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Error" label:@"System Error" value:@"No description"];
    }
}

// Sent to the delegate when the log in screen is dismissed.
- (void)logInViewControllerDidCancelLogIn:(PFLogInViewController *)logInController {
    NSLog(@"User dismissed the logInViewController");
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
