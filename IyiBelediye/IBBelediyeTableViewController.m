//
//  IBBelediyeTableViewController.m
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "IBBelediyeTableViewController.h"
#import "MBProgressHUD.h"
#import "UIColor+Extensions.h"
#import <SDWebImage/UIImageView+WebCache.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "IBAppDelegate.h"

@interface IBBelediyeTableViewController ()

@end

@implementation IBBelediyeTableViewController
@synthesize popupTextView = popupTextView;
- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    
    NSString *mediaTypeInfo = @"";
    NSString *mediaType = [info objectForKey: UIImagePickerControllerMediaType];
    if(CFStringCompare ((__bridge_retained CFStringRef) mediaType, kUTTypeImage, 0)
       == kCFCompareEqualTo)
    {
        UIImage *chosenImage = info[UIImagePickerControllerOriginalImage];
        
        UIImage *smallImage = [self imageScaleWithValue:700 originalImage:chosenImage];
        
        NSData *imageData = UIImageJPEGRepresentation(smallImage, 0.7);
        self.takenPicture = imageData;
        
        mediaTypeInfo = @"Photo";
        
    }
    else
    {
        NSURL *videoURL = [info objectForKey:UIImagePickerControllerMediaURL];
        NSData *videoData = [NSData dataWithContentsOfURL:videoURL];
        
        self.videoURL = videoData;
        
        mediaTypeInfo = @"Video";
        
    }
    
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"Camera" action:@"Use Photo" label:@"User approved media" value:mediaTypeInfo];
    
    [self sendReport];

    [picker dismissViewControllerAnimated:YES completion:nil];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"Camera" action:@"Cancel" label:@"User tapped on Cancel" value:nil];
    [[FlightRecorder sharedInstance] warningWithName:@"Camera" value:@"Cancel"];
    [picker dismissViewControllerAnimated:YES completion:^{
      
    }];
    
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{

    if(buttonIndex == 0)
    {
         [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Action" action:@"Media Selected" label:@"Open Camera" value:@"Camera"];
        
        self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:self.picker animated:YES completion:nil];
    }
    else if(buttonIndex == 1)
    {
         [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Action" action:@"Media Selected" label:@"Open Camera" value:@"PhotoLib"];
        
        self.picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.picker animated:YES completion:nil];
    }
    
    
}

- (IBAction)openCamera:(id)sender {
    
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Kaynak" delegate:self cancelButtonTitle:@"İptal" destructiveButtonTitle:nil otherButtonTitles:@"Kamera",@"Kütüphane", nil];
    
    [sheet showInView:self.view];
    
  
    [[FlightRecorder sharedInstance] trackPageView:@"Camera View"];
    
    
    
    
}


-(void)sendReport
{
    if ((![CLLocationManager locationServicesEnabled])
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
        || ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied))
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hata" message:@"Lokasyon servisini aktif hale getiriniz. Lokasyon servisi olmadan olay rapor edemezsiniz." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
        
        [alert show];
        
        
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Location Service" action:@"Warning" label:@"Disabled" value:@"User didn't allow to location service."];
        
        return;
    }
    else
    {
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Location Service" action:@"Info" label:@"Enabled" value:nil];
    }
    
    
 
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:@"firstTime"] == nil)
    {
        [defaults setObject:@"OK" forKey:@"firstTime"];
        [defaults synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Uyarı" message:@"Raporunuz anlık olarak yetkililere bildirilecektir. Servisi suistimal eden kullanıcıların bilgileri, istenildiği takdirde yetkililer tarafına bildirilebilir. Rapor edeceğiniz veri gerçek midir?" delegate:self cancelButtonTitle:@"Hayır" otherButtonTitles:@"Evet", nil];
        alert.delegate = self;
     
        [alert show];
    }
    else
    {
        
        
        [self openTextView];
    }
    
    
    
}

-(void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if(buttonIndex == 0)
    {
        
        [[FlightRecorder sharedInstance] warningWithName:@"App Usage Rules" value:@"Disagree"];
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"AlertView" action:@"App Usage Rules" label:@"Disagree" value:nil];
    }
    else
    {
        [[FlightRecorder sharedInstance] warningWithName:@"App Usage Rules" value:@"Agree"];
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"AlertView" action:@"App Usage Rules" label:@"Agree" value:nil];
        
        [self openTextView];
    }
}

-(void)sendReportToServer:(NSString *)message
{

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bilgi" message:@"Görüntü seçtiğiniz belediyeye bildirildi." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles: nil];
    [alert show];
    
    
    IBAppDelegate *appDelegate = (IBAppDelegate *)[[UIApplication sharedApplication] delegate];
    
    // If it's not possible to get a location, then return.
    CLLocation *location = appDelegate.locationManager.location;
    if (!location) {
        
        [[FlightRecorder sharedInstance] trackEventWithCategory:@"Location Service" action:@"Warning" label:@"Disabled" value:@"User didn't allow to location service."];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hata" message:@"Lokasyon servisini aktif hale getiriniz. Lokasyon servisi olmadan olay rapor edemezsiniz." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
        
        [alert show];
        
        return;
    }
    
    // Configure the new event with information from the location.
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    
    
    PFGeoPoint *geoPoint = [PFGeoPoint geoPointWithLatitude:coordinate.latitude longitude:coordinate.longitude];
    NSNumber *lat = [NSNumber numberWithDouble:coordinate.latitude];
    NSNumber *llong = [NSNumber numberWithDouble:coordinate.longitude];
    
    
    
    
 
    self.selectedText = [NSString stringWithFormat:@"%@ %@ %@",message, [self.selectedReport objectForKey:@"Hashtag"], [self.selectedBelediye objectForKey:@"twitterAccount"]];
    
    if (self.takenPicture != nil)
    {
        
        
        PFFile *imageFile = [PFFile fileWithName:@"image.jpg" data:self.takenPicture];
        
        PFObject *userPhoto = [PFObject objectWithClassName:@"UserPhoto"];
        [userPhoto setObject:[PFUser currentUser] forKey:@"user"];
        [userPhoto setObject:geoPoint forKey:@"location"];
        [userPhoto setObject:self.selectedBelediye forKey:@"belediye"];
        userPhoto[@"imageFile"] = imageFile;
  
        [userPhoto saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            self.videoURL = nil;
            self.takenPicture = nil;
            
            NSString *url = imageFile.url;
            
            [PFCloud callFunctionInBackground:@"Twitter"
                               withParameters:@{@"status": self.selectedText, @"mediaURL":url, @"lat":lat, @"llong":llong}
                                        block:^(NSArray *results, NSError *error) {
                                            
                                            
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            
                                            if (!error) {
                                                // this is where you handle the results and change the UI.
                                                
                                                [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Actions" action:@"Twitter Share Photo" label:@"Success" value:nil];
                                                
                                            }
                                            else
                                            {
                                                [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Actions" action:@"Twitter Share Photo" label:@"Fail" value:error.description];
                                                
                                                [[FlightRecorder sharedInstance] warningWithName:@"Twitter Share Video Photo" value:error.description];
                                            }
                                            
                                            [self.navigationController popToRootViewControllerAnimated:YES];
                                        }];
            
        }];
        
    }
    else
    {
        
        PFFile *videoFile = [PFFile fileWithName:@"video.mov" data:self.videoURL];
        
        PFObject *userVideo = [PFObject objectWithClassName:@"UserVideo"];
        [userVideo setObject:[PFUser currentUser] forKey:@"user"];
        [userVideo setObject:geoPoint forKey:@"location"];
        [userVideo setObject:self.selectedBelediye forKey:@"belediye"];
        userVideo[@"videoFile"] = videoFile;
        
        self.videoURL = nil;
        self.takenPicture = nil;
        
        [userVideo saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            
            NSString *url = videoFile.url;
            
            [PFCloud callFunctionInBackground:@"Twitter"
                               withParameters:@{@"status": self.selectedText, @"mediaURL":url, @"lat":lat, @"llong":llong}
                                        block:^(NSArray *results, NSError *error) {
                                            [MBProgressHUD hideHUDForView:self.view animated:YES];
                                            
                                            if (!error) {
                                                // this is where you handle the results and change the UI.
                                                
                                                [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Actions" action:@"Twitter Share" label:@"Success" value:nil];
                                                
                                            }
                                            else
                                            {
                                                [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Actions" action:@"Twitter Share Video" label:@"Fail" value:error.description];
                                                
                                                [[FlightRecorder sharedInstance] warningWithName:@"Twitter Share Video Fail" value:error.description];
                                            }
                                            
                                            [self.navigationController popToRootViewControllerAnimated:YES];
                                        }];
            
        }];
    }
    
}

-(void)openTextView
{
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"Flow Actions" action:@"Text View Opened" label:@"User Custom Text" value:nil];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"Gönder" style:UIBarButtonItemStylePlain target:self action:@selector(closeTextView)];
    popupTextView = [[YIPopupTextView alloc] initWithPlaceHolder:@"Belediyeye iletilecek mesaj" maxCount:50 buttonStyle:YIPopupTextViewButtonStyleNone];
    popupTextView.delegate = self;
    popupTextView.caretShiftGestureEnabled = YES;   // default = NO
   
    //popupTextView.editable = NO;                  // set editable=NO to show without keyboard
    
    //[popupTextView showInView:self.view];
    [popupTextView showInViewController:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    
    if([defaults objectForKey:@"firstTimeMessage"] == nil)
    {
        [defaults setObject:@"OK" forKey:@"firstTimeMessage"];
        [defaults synchronize];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Bilgilendirme" message:@"Bildirime eklemek istediğiniz mesajınız varsa buraya yazabilirsiniz. İsterseniz kendi Twitter hesabınızı 'Mention' olarak ekleyerek bildiriminize kendinizi de dahil edebilirsiniz. Bildirimler @iyibelediye hesabı üzerinden yapılmaktadır, bu hesap üzerinden işlemleri takip edebilirsiniz." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];        
        [alert show];
    }
}

-(void)closeTextView
{
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"UI Actions" action:@"Dismiss" label:@"TextView" value:nil];
    [popupTextView dismiss];
}

-(void)popupTextView:(YIPopupTextView *)textView didDismissWithText:(NSString *)text cancelled:(BOOL)cancelled
{
    if (!cancelled) {
        [self sendReportToServer:text];
    }
}


#pragma mark -
#pragma mark UIImage Scale
-(UIImage*)imageScaleWithImage: (UIImage*) sourceImage percent: (float) percent
{
    
    
    float newWidth = sourceImage.size.width * percent / 100;
    float newHeight = sourceImage.size.height * percent / 100;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToHeight: (float) i_height
{
    float oldHeight = sourceImage.size.height;
    float scaleFactor = i_height / oldHeight;
    
    float newWidth = sourceImage.size.width * scaleFactor;
    float newHeight = oldHeight * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage*)imageWithImage: (UIImage*) sourceImage scaledToWidth: (float) i_width
{
    float oldWidth = sourceImage.size.width;
    float scaleFactor = i_width / oldWidth;
    
    float newHeight = sourceImage.size.height * scaleFactor;
    float newWidth = oldWidth * scaleFactor;
    
    UIGraphicsBeginImageContext(CGSizeMake(newWidth, newHeight));
    [sourceImage drawInRect:CGRectMake(0, 0, newWidth, newHeight)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

-(UIImage *)imageScaleWithValue:(CGFloat)maxValue originalImage:(UIImage *)img
{
    UIImage *imgSized;
    if(img.size.width < img.size.height)
    {
        
        if(img.size.height > maxValue)
            imgSized = [self imageWithImage:img scaledToHeight:maxValue];
        else {
            imgSized = img;
        }
        
        return imgSized;
    }
    else if (img.size.width > img.size.height ){
        
        if(img.size.width > maxValue)
            imgSized = [self imageWithImage:img scaledToWidth:maxValue];
        else {
            imgSized = img;
        }
        return imgSized;
    }
    else {
        
        if(img.size.height > maxValue)
            imgSized = [self imageWithImage:img scaledToHeight:maxValue];
        else {
            imgSized = img;
        }
        
        return imgSized;
        
    }
    
    
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (popupTextView) {
        [popupTextView dismissWithCancelled:YES];
    }
}
- (void)viewDidLoad
{
    [[FlightRecorder sharedInstance] trackPageView:@"Belediye List Page"];
    
    self.picker = [[UIImagePickerController alloc] init];
    self.picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    self.picker.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                              UIImagePickerControllerSourceTypeCamera];
    self.picker.delegate = self;
    self.title = @"Belediye Seçin";
    self.belediyeler = [NSMutableArray array];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    PFQuery *query = [PFQuery queryWithClassName:@"Belediyeler"];
    [query orderByAscending:@"Order"];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            
            [self.belediyeler addObjectsFromArray:objects];
            
            [self.tableView reloadData];
            
            
            [MBProgressHUD hideHUDForView:self.view animated:YES];
            
            
        } else {
            NSString *errorStr = [NSString stringWithFormat:@"Error: %@ %@", error, [error userInfo]];
            [[FlightRecorder sharedInstance] logAPIRequestWithName:@"Load Belediyeler" url:@"Belediyeler" httpMethod:@"GET" requestBody:nil requestHeaders:nil responseStatusCode:500 responseString:errorStr responseHeaders:nil];
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.belediyeler.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        return 80;
    
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    
    PFObject *object = [self.belediyeler objectAtIndex:indexPath.row];
    
    UITableViewCell *cell;
    
    
    
    cell = [self.tableView dequeueReusableCellWithIdentifier:@"belediyeCell"];
    
    
    UILabel *btn = (UILabel *)[cell viewWithTag:101];
    btn.text = [object objectForKey:@"belediyeAdi"];
    

    
     UIImageView *imgView = (UIImageView *)[cell viewWithTag:100];
    [imgView setImageWithURL:[NSURL URLWithString:[object objectForKey:@"Logo"]]];
    
    return cell;
    
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     [[FlightRecorder sharedInstance] trackEventWithCategory:@"UI Actions" action:@"TableView Row" label:@"Belediye Selected" value:[self.selectedBelediye objectForKey:@"belediyeAdi"]];
    
    self.selectedBelediye =[self.belediyeler objectAtIndex:indexPath.row];
    [self openCamera:nil];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
