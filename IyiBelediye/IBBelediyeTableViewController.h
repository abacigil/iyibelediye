//
//  IBBelediyeTableViewController.h
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YIPopupTextView.h"
@interface IBBelediyeTableViewController : UITableViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,YIPopupTextViewDelegate>
@property(nonatomic, strong) NSMutableArray *belediyeler;
@property(nonatomic, strong) UIImagePickerController *picker;
@property(nonatomic, strong) NSData *takenPicture;
@property(nonatomic, strong) NSData *videoURL;
@property(nonatomic, strong) PFObject *selectedReport;
@property(nonatomic, strong) NSString *selectedText;
@property(nonatomic, strong) PFObject *selectedBelediye;
@property(nonatomic, strong) YIPopupTextView* popupTextView;
@end
