//
//  main.m
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "IBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([IBAppDelegate class]));
    }
}
