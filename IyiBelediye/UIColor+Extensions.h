//
//  UIColor+HexColor.h
//  MekanistiOS
//
//  Created by Eren Baydemir on 26.05.2012.
//  Copyright (c) 2012 Mekanist.net. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (HexColor)

+ (UIColor *) kudretBlue;
+ (UIColor *) clickableBlue;
+ (UIColor *) clickableBlueHighlighted;
+ (UIColor *) cellBorder;
+ (UIColor *) cellDarkBorder;

+ (UIColor*) colorWithHEX:(NSString*)hexString;

@end
