//
//  IBAppDelegate.m
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import "IBAppDelegate.h"

@implementation IBAppDelegate
@synthesize locationManager = _locationManager;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [Parse setApplicationId:@"mXBtbDhybMxCRNWwuNpQtJYTsaal1nJFq8s9OL6o"
                  clientKey:@"vG0h2IY2P9vruSFZHxxnUYZNEGLR09U5qJBv9CcE"];
    
    [PFFacebookUtils initializeFacebook];
    [PFTwitterUtils initializeWithConsumerKey:@"JeVQM9NNS4hN4VEut58hzR3F7" consumerSecret:@"BIh6CoMT2tby3SWbCxg1e3B9pZUhZGKKvJ2c5MYACrazpFd3UR"];
    
    // Set default ACLs
    PFACL *defaultACL = [PFACL ACL];
    [defaultACL setPublicReadAccess:YES];
    [PFACL setDefaultACL:defaultACL withAccessForCurrentUser:YES];
    
    
    [[FlightRecorder sharedInstance] setAccessKey:@"95a17cfc-84b5-47d7-a873-1b585ee89770" secretKey:@"c11f0187-3524-445f-80d3-9faa7695809e"];
    [[FlightRecorder sharedInstance] setQuality:FE_QUALITY_HIGH];
    [[FlightRecorder sharedInstance] setRecordingFPS:15];
    [[FlightRecorder sharedInstance] setShouldStartLocationManager:YES];
    [[FlightRecorder sharedInstance] startFlight];
    
    [self.locationManager startUpdatingLocation];
    
    // Override point for customization after application launch.
    return YES;
}

- (CLLocationManager *)locationManager {
	
    if (_locationManager != nil) {
		return _locationManager;
	}
	
	_locationManager = [[CLLocationManager alloc] init];
    _locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
    _locationManager.delegate = self;
    
	
	return _locationManager;
}


// Facebook oauth callback
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Facebook" label:@"From Safari" value:nil];
    
    return [PFFacebookUtils handleOpenURL:url];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
    [[FlightRecorder sharedInstance] trackEventWithCategory:@"Login" action:@"Facebook" label:@"From App" value:sourceApplication];
    
    return [PFFacebookUtils handleOpenURL:url];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Handle an interruption during the authorization flow, such as the user clicking the home button.
    [FBSession.activeSession handleDidBecomeActive];
    
    
    if ([PFUser currentUser]) {
        
        if([PFUser currentUser].username != nil)
        {
            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].username];
        }
        else
        {
            [[FlightRecorder sharedInstance] setSessionUserID:[PFUser currentUser].email];
        }
        
    }
}
							
- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
