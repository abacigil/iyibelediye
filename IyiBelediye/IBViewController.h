//
//  IBViewController.h
//  IyiBelediye
//
//  Created by Davut Can Abacigil on 30/06/14.
//  Copyright (c) 2014 Kaya Kahveci. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IBViewController : UITableViewController <PFLogInViewControllerDelegate>
@property(nonatomic, strong) NSMutableArray *reports;
@end
