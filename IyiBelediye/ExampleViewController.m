#import "ExampleViewController.h"
#import "IntroControll.h"

@implementation ExampleViewController

- (id)init
{
    self = [super initWithNibName:nil bundle:nil];
   
    return self;
}

-(void)close
{
    //[[FlightRecorder sharedInstance] trackEventWithCategory:@"UI Actions" action:@"Button" label:@"Skip Intro" value:nil];
    [self dismissViewControllerAnimated:YES completion:^{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Hatırlatma" message:@"Kişisel bilgilerinizin gizliliğine önem veriyoruz. Bilgileriniz kesinlikle paylaşılmamaktadır. Facebook/Twitter ile giriş yapılması, sadece sistemin kötüye kullanılmaması için gereklidir. Giriş yaptığınız hesabınızla hiçbir işlem yapılmamaktadır. Bildirimler kendi hesabımız üzerinden yapılmaktadır. \n\n App Store'da uygulamamıza puan vererek destek olabilirsiniz." delegate:nil cancelButtonTitle:@"Tamam" otherButtonTitles:nil];
        [alert show];
    }];

}

- (void) loadView {
    [super loadView];
    
    IntroModel *model1 = [[IntroModel alloc] initWithTitle:@"İyi Belediye" description:@"Uygulama gördüğünüz aksaklıkları ilgili belediyelere anında bildirip çözüme kavuşturmak amacındadır." image:@"photo1.jpg"];
    
    IntroModel *model2 = [[IntroModel alloc] initWithTitle:@"Kategori Belirle" description:@"Bildiriminizin daha iyi sonuçlanması için bir kategori belirlemelisiniz." image:@"photo2.png"];
    
    IntroModel *model3 =[[IntroModel alloc] initWithTitle:@"Belediyeye Bildir" description:@"İlgili belediyeyi seçip fotoğraf/video gönderip varsa mesajınızı iletiniz. Bilgileriniz asla paylaşılmaz." image:@"photo3.png"];
    
    self.view = [[IntroControll alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) pages:@[model1, model2, model3]];
    
    UIButton  *btn = [UIButton buttonWithType:UIButtonTypeSystem];
    btn.backgroundColor = [UIColor whiteColor];
    [btn setTitle:@"Kapat" forState:UIControlStateNormal];
    
    btn.frame = CGRectMake(0, [UIScreen mainScreen].bounds.size.height-30, 320, 30);
    [btn addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn];
}

- (void)viewDidLoad
{
    //[[FlightRecorder sharedInstance] trackPageView:@"Intro View"];
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
